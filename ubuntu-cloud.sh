#!/usr/bin/env bash
function create_image {
  role=$1
  ip=$2

  # This is already in qcow2 format.
  path=./disk/root
  img=ubuntu-20.04-server-cloudimg-amd64.img

  role_path=./disk/$role
  if [ ! -f "$path/$img" ]; then
    mkdir -p $path
    wget "https://cloud-images.ubuntu.com/releases/20.04/release/${img}" --directory-prefix=$path

    # sparse resize: does not use any extra space, just allows the resize to happen later on.
    # https://superuser.com/questions/1022019/how-to-increase-size-of-an-ubuntu-cloud-image
  fi

  if [ ! -f "$role_path/$role/$role.img" ]; then
    mkdir -p $role_path
    cp $path/$img $role_path/$role.img
    qemu-img resize -q "$role_path/$role.img" +12G
  fi

  user_data=user-data-$role.img
  if [ ! -f "$role_path/$user_data" ]; then
    # For the password.
    # https://stackoverflow.com/questions/29137679/login-credentials-of-ubuntu-cloud-server-image/53373376#53373376
    # https://serverfault.com/questions/920117/how-do-i-set-a-password-on-an-ubuntu-cloud-image/940686#940686
    # https://askubuntu.com/questions/507345/how-to-set-a-password-for-ubuntu-cloud-images-ie-not-use-ssh/1094189#1094189
    #https://fabianlee.org/2020/02/23/kvm-testing-cloud-init-locally-using-kvm-for-an-ubuntu-cloud-image/
    rm $role_path/network_config_static-$role.cfg $role_path/cloud_init-$role.cfg
    cat >$role_path/cloud_init-$role.cfg <<INIT
#cloud-config
# password: ubuntu
# chpasswd: { expire: False }
# ssh_pwauth: True
version: 2
hostname: $role
fqdn: $role.example.com
manage_etc_hosts: true
users:
  - name: ubuntu
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups: users, admin
    home: /home/ubuntu
    shell: /bin/bash
    lock_passwd: false
    passwd: \$6\$rounds=4096\$xLSxtTovu0C0BZqb\$EG0K1NOAV5ppLxnHK0Yba/spFKqRjN1.ibtWHiyiMlKYR2zfx2ThW5DKs0Vy1C7krLS1NDP.0gVbg1f3qpMOz/
# only cert auth via ssh (console access can still login)
ssh_pwauth: true
disable_root: false
packages:
  - qemu-guest-agent
# written to /var/log/cloud-init-output.log
final_message: "The system is finally up, after $UPTIME seconds"
chpasswd: { expire: False }
password: ubuntu
# chpasswd:
#   list: |
    #  ubuntu:ubuntu
INIT
    cat >$role_path/network_config_static-$role.cfg <<NET
version: 2
ethernets:
  enp0s3:
     dhcp4: false
     # default libvirt network
     addresses: [ 10.0.0.$ip/24 ]
     gateway4: 10.0.0.1
     nameservers:
       addresses: [ 10.0.0.1,8.8.8.8 ]
  enp0s4:
     dhcp4: false
     addresses: [ 10.1.0.$ip/24 ]
  enp0s5:
     dhcp4: false
     addresses: [ 10.2.0.$ip/24 ]
  enp0s6:
    dhcp4: false
    addresses: [ 10.3.0.$ip/24 ]
NET
    cloud-localds -v --network-config=$role_path/network_config_static-$role.cfg  $role_path/$user_data $role_path/cloud_init-$role.cfg
    # cloud-localds "$user_data" user-data
  fi

  # qemu-system-x86_64 \
  #   -drive "file=${img},format=qcow2" \
  #   -drive "file=${user_data},format=raw" \
  #   -device rtl8139,netdev=net0 \
  #   -enable-kvm \
  #   -m 2G \
  #   -netdev user,id=net0 \
  #   -serial mon:stdio \
  #   -smp 2 \
  #   -vga virtio \
  # ;
  echo  $role_path
}
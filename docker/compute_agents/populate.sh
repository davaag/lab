#!/bin/bash 
for folder in nova neutron
do
  rm -Rf ${folder}/ || true
  mkdir -p ${folder}/ || true
  
  while IFS= read -r -d '' -u 9
  do
      DIRNAME=$(dirname ${REPLY})
      if [ "${DIRNAME}" != "." ]; then
        mkdir -p ${folder}/${DIRNAME}
      fi
      frep ${folder}_templates/${REPLY}:${folder}/${REPLY%.*in} --load "${folder}.yaml" --strict
  done 9< <( find ${folder}_templates -type f -printf '%P\0' )
done

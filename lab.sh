#!/bin/bash  -xe

# set -o errexit
# set -o pipefail
# set -o nounset
# set -o xtrace
#https://developers.redhat.com/blog/2018/10/22/introduction-to-linux-interfaces-for-virtual-networking/
#https://gist.github.com/mtds/4c4925c2aa022130e4b7c538fdd5a89f
#https://linux-blog.anracom.com/2016/02/02/fun-with-veth-devices-linux-virtual-bridges-kvm-vmware-attach-the-host-and-connect-bridges-via-veth/
#https://gabhijit.github.io/linux-virtual-interfaces.html
#https://sarathblogs.blogspot.com/2015/02/quickbite-tap-vs-veth.html
#https://www.redhat.com/en/blog/deep-dive-virtio-networking-and-vhost-net

OPT=${1:-}
LEVEL=${2:-1}

declare -A bridgePlan=( ["br-mgmt"]="10.0.0.1/24" ["br-storage"]="10.1.0.1/24" ["br-vxlan"]="10.2.0.1/24" ["br-vlan"]="10.3.0.1/24")
declare -A ifaces=(["cntrl0"]="br-mgmt br-storage br-vxlan br-vlan")


function bridgeSetup {
    local name=$1
    local range=$2
    local iface=$3

    sudo ip link add name ${name} type bridge || true
    sudo ip addr add ${range} dev ${name} || true
    sudo ip link set ${name} up || true
    sudo dnsmasq --interface=${name} -K --bind-dynamic --dhcp-range=${range::-5}".2",${range::-5}".254",255.255.255.0 
}

function ifaceSetup {  
    local iface=$1
    # shift
    local bridges=("${@:2}")
    local counter="0"

    sudo modprobe tun || true
    for bridge in ${bridges[@]}
     do
        sudo ip tuntap add dev ${iface}-eth${counter} mode tap 
        sudo ip link set ${iface}-eth${counter} up promisc on || true
        sudo ip link set ${iface}-eth${counter} master ${bridge} || true

        ((counter=counter+1))
     done
}

function setupNet {
    
    sudo sysctl net.ipv4.ip_forward=1
    sudo sysctl net.ipv6.conf.default.forwarding=1
    sudo sysctl net.ipv6.conf.all.forwarding=1
    sudo sysctl net.bridge.bridge-nf-call-arptables=0
    sudo sysctl net.bridge.bridge-nf-call-iptables=1

    sudo iptables -t nat -A POSTROUTING -o enp4s0 -j MASQUERADE
    sudo iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
    sudo iptables -A FORWARD -i br-mgmt -o enp4s0 -j ACCEPT
    sudo iptables -A FORWARD -i br-vlan -o enp4s0 -j ACCEPT

}

function postSetupNet {
    sudo ip route add 10.100.0.0/16 via 10.3.0.5
    sudo mount /dev/sda1 /mnt
}


case $OPT in
   -n )
   echo "Setup common config..."
   setupNet

   for links in "${!bridgePlan[@]}"
    do
        echo "Setup bridge ${links}..."
        bridgeSetup ${links} ${bridgePlan[${links}]}
    done 

   for iface in "${!ifaces[@]}"
    do
        ifaceSetup ${iface} "${ifaces[${iface}]}"
    done

    postSetupNet

    ./lauch_nodes.sh -o

    docker run -d --name compute_agent --add-host ryzen:10.0.0.1 --add-host cntrl0:10.0.0.3 -v /dev:/dev -v /mnt:/var/lib/nova --privileged --net=host --rm -it compute_agent systemd 

   ;;
   -iu )
   echo "Creating new Ubuntu 10 img..."
   echo "Do you wish to install this program?"
   select yn in "Yes" "No"; do
    case $yn in
        Yes ) install_ubuntu; break;;
        No ) exit;;
    esac
   done
   ;;
   -r )
   echo "Running windows image"
   run 
   ;;
   * )
   echo -e "-i[w/u] to install windows or ubuntu\n-r to run windows image\n-n to setup configuration"
esac


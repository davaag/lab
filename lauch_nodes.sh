#!/usr/bin/env bash 
set -xv

OPT=${1:-}

source ./ubuntu-cloud.sh

pci_address="1022 7901"
pci_bus="0000:07:00.0"

function unlink_pci {
     echo "${pci_bus}" | sudo tee -a /sys/bus/pci/devices/${pci_bus}/driver/unbind >/dev/null
     echo "${pci_address}" | sudo tee -a /sys/bus/pci/drivers/vfio-pci/new_id >/dev/null
}

function launch_qemu {
    local drive=$1
    local mem=$2
    local cpu=$3
    local ip=$4
    local path_old=$5

    if [[ -z "${path_old}" ]];
    then
      local path=$(create_image $drive $ip)
    else 
      local path=${path_old}
    fi

        # -device vfio-pci,host=07:00.0 \
    sudo prlimit --memlock=unlimited --\ 
    qemu-system-x86_64 -enable-kvm -m $mem -drive file=$path/$drive.img,format=qcow2 -drive file=$path/user-data-$drive.img,format=raw\
        -machine q35 -device intel-iommu,caching-mode=on  \
        -cpu host -smp cpus=$cpu \
        -netdev tap,id=mynet1,ifname=$drive-eth0,script=no,downscript=no -device e1000,netdev=mynet1,mac=$(echo $drive-eth0|md5sum|sed 's/^\(..\)\(..\)\(..\)\(..\)\(..\).*$/02:\1:\2:\3:\4:\5/')\
        -netdev tap,id=mynet2,ifname=$drive-eth1,script=no,downscript=no -device e1000,netdev=mynet2,mac=$(echo $drive-eth1|md5sum|sed 's/^\(..\)\(..\)\(..\)\(..\)\(..\).*$/02:\1:\2:\3:\4:\5/')\
        -netdev tap,id=mynet3,ifname=$drive-eth2,script=no,downscript=no -device e1000,netdev=mynet3,mac=$(echo $drive-eth2|md5sum|sed 's/^\(..\)\(..\)\(..\)\(..\)\(..\).*$/02:\1:\2:\3:\4:\5/')\
        -netdev tap,id=mynet4,ifname=$drive-eth3,script=no,downscript=no -device e1000,netdev=mynet4,mac=$(echo $drive-eth3|md5sum|sed 's/^\(..\)\(..\)\(..\)\(..\)\(..\).*$/02:\1:\2:\3:\4:\5/')\
        -vga virtio -device virtio-serial-pci \
        -spice port=590$ip,addr=127.0.0.1,disable-ticketing,disable-ticketing \
        -device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0 -chardev spicevmc,id=spicechannel0,name=vdagent &

}


declare -A nodes=(["cntrl0"]="6000 2 3")

drivers_in_use=$(lspci -nnk -d $(echo ${pci_address} | tr " " ":") -s ${pci_bus} | grep vfio)

# if [[ -z "${drivers_in_use}" ]];
  # then
    # unlink_pci
# fi


case $OPT in
  -n )
   echo "Launch new instance..."
   for node in "${!nodes[@]}"
    do
      launch_qemu ${node} ${nodes[${node}]}
   done
   ;;

  -o )
   echo "Launch old instance..."
   for node in "${!nodes[@]}"
    do
    launch_qemu ${node} ${nodes[${node}]} ./disk/${node}
   done

   ;;

  * )
   echo -e "-n to launch a new instance\n-o to launch old images"
esac